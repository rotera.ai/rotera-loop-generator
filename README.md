# Rotera Loop Generator

The Rotera Loop Generator is a CLI for generating Olive Help Loops.

It follows the Loop project structure used by 
Rotera AI [https://www.rotera.ai](https://www.rotera.ai).

## Install the cli

To install the Rotera Loop Generator, you need to tell the Node `npm` tool where to find it.

If you already have a `.npmrc` in your home directory, add the following line.

```
@rotera.ai:registry=https://gitlab.com/api/v4/projects/30692354/packages/npm/
```

If you don’t a `.npmrc` in your home directory, create the file `.npmrc` in your 
home directory and set its contents to:

```
@rotera.ai:registry=https://gitlab.com/api/v4/projects/30692354/packages/npm/
```

Once you have a `.npmrc` with the proper contents, type the command

```
npm install -g @rotera.ai/rotera-loop-generator
```

## Initialize a loop project

Go to the root folder where you want your project to appear. Then type the following command.

```
rlg init
```

You will be queried for a loop name and version. Say your loop name is `My First Loop`.
The tool will create a directory called `my-first-loop` in the folder you are in.

Go into that folder and type `yarn install` to get your dependencies installed.

