import fs from 'fs-extra';
import path from 'path';
const Handlebars = require("handlebars");

/**
 * TemplateConfig
 *
 * The configuration for a template
 */
export type TemplateConfig = {

  /**
   * loopName
   *
   * The name of the loop.
   */
  loopName: string

  /**
   * loopVersion
   *
   * The version of the loop.
   */
  loopVersion: string

  /**
   * loopHomeDirectory
   *
   * The home directory name for the loop.
   */
  loopHomeDirectory: string

  /**
   * loopNpmName
   *
   * The name the loop will have in the NPM package.
   */
  loopNpmName: string
}

/**
 * Template
 *
 * A description of a file to be generated.
 */
export type Template = {

  /**
   * source
   *
   * The source of the template.
   */
  source: string

  /**
   * destination
   *
   * The destination of the template
   */
  destination: string
}

/**
 * baseTemplates
 *
 * The base templates that every project will have.
 */
export const baseTemplates: Template[] = [
  {
    source: "base/gitignore.hbs",
    destination: ".gitignore",
  },
  {
    source: "base/README.md.hbs",
    destination: "README.md",
  },
  {
    source: "base/prettierignore.hbs",
    destination: ".prettierignore",
  },
  {
    source: "base/prettierrc.hbs",
    destination: ".prettierrc",
  },
  {
    source: "base/env-local.hbs",
    destination: ".env-local",
  },
  {
    source: "base/editorconfig.hbs",
    destination: ".editorconfig",
  },
  {
    source: "base/eslintignore.hbs",
    destination: ".eslintignore",
  },
  {
    source: "base/eslintrc.hbs",
    destination: ".eslintrc",
  },
  {
    source: "base/npmrc.hbs",
    destination: ".npmrc",
  },
  {
    source: "base/package.json.hbs",
    destination: "package.json",
  },
  {
    source: "base/webpack.config.ts.hbs",
    destination: "webpack.config.ts",
  },
  {
    source: "base/jest.config.json.hbs",
    destination: "jest.config.json",
  },
  {
    source: "base/tsconfig.json.hbs",
    destination: "tsconfig.json",
  },
  {
    source: "base/config/custom-environment-variables.json.hbs",
    destination: "config/custom-environment-variables.json",
  },
  {
    source: "base/config/dev.json.hbs",
    destination: "config/dev.json",
  },
  {
    source: "base/config/default.json.hbs",
    destination: "config/default.json",
  },
  {
    source: "base/config/test.json.hbs",
    destination: "config/test.json",
  },
  {
    source: "base/tests/README.md.hbs",
    destination: "tests/README.md",
  },
  {
    source: "base/tests/example.test.ts.hbs",
    destination: "tests/example.test.ts",
  },
  {
    source: "base/src/modules.ts.hbs",
    destination: "src/modules.ts",
  },
  {
    source: "base/src/global.ts.hbs",
    destination: "src/global.ts",
  },
  {
    source: "base/src/index.ts.hbs",
    destination: "src/index.ts",
  },
  {
    source: "base/src/routes.js.hbs",
    destination: "src/routes.js",
  },
  {
    source: "base/src/static/i18n/README.md.hbs",
    destination: "src/static/i18n/README.md",
  },
  {
    source: "base/src/static/i18n/lang.ts.hbs",
    destination: "src/static/i18n/lang.ts",
  },
  {
    source: "base/src/static/i18n/en.ts.hbs",
    destination: "src/static/i18n/en.ts",
  },
  {
    source: "base/src/eventBus.ts.hbs",
    destination: "src/eventBus.ts",
  }
];

/**
 * gitlabCiCdTemplates
 *
 * Templates for when using Gitlab CI/CD.
 */
export const gitlabCiCdTemplates: Template[] = [
  {
    source: "base/gitlab-ci.yml.hbs",
    destination: ".gitlab-ci.yml",
  },
  {
    source: "base/releaserc.json.hbs",
    destination: ".releaserc.json",
  },
];

/**
 * generateTemplates
 *
 * @param templateConfig The configuration for the template
 * @param templates The templates to be rendered
 */
export async function generateTemplates(templateConfig: TemplateConfig, templates: Template[]) {
  const rootDirectoryPath = process.cwd() + '/' + templateConfig.loopHomeDirectory
  console.log(`Creating directory ${rootDirectoryPath}`);

  await fs.mkdirp(rootDirectoryPath)

  for (const template of templates) {
    console.log(`Writing ${template.destination}`)

    const handlebarsTemplate = Handlebars.compile(
      fs.readFileSync(__dirname + '/templates/' + template.source, 'utf8'));
    const relativePath = template.destination.split("/")

    const fullDirectoryPath = path.join(rootDirectoryPath, ...relativePath.slice(0, -1))
    await fs.mkdirp(fullDirectoryPath)

    const fullFilePath = path.join(fullDirectoryPath, ...relativePath.slice(-1));
    fs.writeFileSync(fullFilePath, handlebarsTemplate(templateConfig));
  }
}