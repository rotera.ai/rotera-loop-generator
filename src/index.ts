#!/usr/bin/env node

import {initializeLoopCommand} from "./initializeLoopCommand";

//const chalk = require('chalk');
//const clear = require('clear');
//const figlet = require('figlet');
const program = require('commander');

program
  .version('0.0.1')
  .description("A CLI for generating Olive Helps Loops. Brought to you by Rotera, AI.")
  .option('-n, --noninteractive', 'Run the tool non-interactively')
  .command('init')
  .action(() => initializeLoopCommand())
  .parse(process.argv);
