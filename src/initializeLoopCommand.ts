import {baseTemplates, generateTemplates, TemplateConfig} from "./templates";
import inquirer from "inquirer";

const program = require('commander');
const _ = require('lodash')

/**
 * initializeLoopCommand
 *
 * The command to initialize a loop.
 */
export function initializeLoopCommand() {
  console.log("Initializing your loop")

  const programOpts = program.opts();
  if (programOpts.noninteractive) {
    (async() => initializeLoopNonInteractively(programOpts))();
  } else {
    (async() => initializeLoopInteractively(programOpts))();
  }
}

/**
 * initializeLoopNonInteractively
 *
 * Initialize a loop non-interactively
 *
 * @param programOpts The program options
 */
async function initializeLoopNonInteractively(programOpts: any) {
  console.error("The CLI does not work non-interactively yet")
}

/**
 * initializeLoopInteractively
 *
 * Initialize a loop interactively
 *
 * @param programOpts The program options
 */
async function initializeLoopInteractively(programOpts: any) {
  const answers = inquirer.prompt([
    {
      name: 'loopName',
      type: 'string',
      message: 'What is the name of your loop?'
    },
    {
      name: 'loopVersion',
      type: 'string',
      message: 'What is the initial version of your loop?',
      default: 'v0.0.1'
    },
  ]).then(async (answers) => {
    const kebabCaseName = _.kebabCase(answers.loopName);
    const templateConfig: TemplateConfig = {
      loopName: answers.loopName,
      loopVersion: answers.loopVersion,
      loopHomeDirectory: kebabCaseName,
      loopNpmName: kebabCaseName
    }

    await generateTemplates(templateConfig, baseTemplates)

    console.log("Your loop initialization is complete! Happy Looping!");
  });
}