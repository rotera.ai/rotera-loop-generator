# Loop starter project for typescript

# TOC
- [Building a local loop](#building-a-local-loop)
- [Repository configuration](#repository-configuration)
- [Rotera's private NPM registry](#roteras-private-npm-registry)
- [Commit messages](#commit-messages)
- [Tooling](#tooling)
- [Prettier configuration](#prettier-configuration)
- [Gotchas](#gotchas)
- [Best practices & things to consider](#best-practices--things-to-consider)

# Building a local loop
- Install node 14
  - Install [nvm](https://github.com/nvm-sh/nvm)
  - Then run `nvm install 14; nvm use 14`
- Download Olive Helps and register as a loop author from the [official getting started guide](https://oliveai.dev/guides/getting-started).
- Ensure the loop's [permissions](https://github.com/open-olive/loop-development-kit/tree/main/ldk/javascript#loop-permissions) are configured for each aptitude in `package.json`
- Duplicate `.env-local` and rename it to `.env` and add all your ENV variables. This file should never be committed to the repo.
- Set `RUN_JOB_RELEASES` to `'true'` in `.gitlab-ci.yml`
- Install node modules via `yarn i` from the root of your loop directory.
- Build the loop by running `yarn watch` or `yarn build` for a single build.
- Open Olive Helps
- Click **Loop Library** in the footer of Olive Helps
- Click **Local Loops** in the sidebar of Loop Library
- Click **Install Local Loop** in the top right corner of the Loop Library
- Add a loop name
- Select the loop's `dist` directory for **Local Directory**. This folder is auto-generated from `yarn build` and `yarn watch`.
- Click `Install Local Loop`
- Create a symlink so Olive Helps auto-updates the loop with new changes. We'll use the utility in Rotera's private package.
  - If you previously ran `yarn watch`, exit the script.
  - Run `npm config set -- '//gitlab.com/api/v4/projects/26976073/packages/npm/:_authToken' "<ROTERA_NPM_TOKEN>"`. This token can be found in Rotera's password manager.
  - Run `yarn symlink`
  - Run `yarn build` or `yarn watch`
  - Now every time you click **Refresh** in Olive Helps' Loop Library, your loop will update with the latest changes.
  
# Repository configuration

## Merge requests
Configure some "best practices" for the repo and CI/CD.

Location: Settings -> General

**Merge method**
- Fast-forward merge

**Merge Options**
- Enable merged result pipelines. This makes sure GitLab's CI works with Rotera's NPM Registry and ENV variables.

**Squash commits when merging**
- Encourage

**Merge checks**
- Pipelines must succeed
- All discussions must be resolved

## Variables
We need to set up some variables so `.gitlab-ci.yml` works properly.

Location: Settings -> CI/CD

- Add `GITLAB_TOKEN` for [semantic releases](https://github.com/semantic-release/gitlab#gitlab-authentication). This token can be created under **Settings -> Access Tokens**. GitLab only displays tokens once, so make sure to store it in a password manager after you've copied it. 
  - Name the token `Pipeline`
  - It should have scopes `api` and `write_repository`.
- Add `ROTERA_REGISTRY_TOKEN`. The variable MUST be unprotected to work in MRs. This token can be found in Rotera's password manager. It's the same token used in the **Building a local loop** section.

# Rotera's private NPM registry
Utilities that that help us resolve common tasks more efficiently.
- [View Rotera NPM Registry README](https://gitlab.com/rotera.ai/rotera-npm-registry/-/tree/main)
- [Learn more](https://docs.gitlab.com/ee/user/packages/) about packages and registries.

# Commit messages

We use **[semantic-release](https://github.com/semantic-release/semantic-release)** to generate version numbers and releases by parsing our commit messages in the [Angular JS commit message format](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format).

Here is an example of the release type that will be done based on a commit messages:

| Commit message                                                                                                                                                                                   | Release type               |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |

See the [semantic-release](https://github.com/semantic-release/semantic-release) docs for more information.

# Tooling
- `yarn` Our package manager of choice. Has a very similar command structure to npm. See [yarn getting started guide](https://yarnpkg.com/getting-started).
- `tsc` Transpiles TS files according to your tsconfig.
- `eslint` ensures our team has consistent code and avoids bugs.
- `prettier` ensures our team is coding with a consistent style.

# Prettier configuration

## Jetbrains
- [Prettier vs Linter](https://prettier.io/docs/en/comparison.html)
- [Jetbrains installation](https://prettier.io/docs/en/webstorm.html#using-prettier-with-eslint)
- [Jetbrains configuration](https://plugins.jetbrains.com/plugin/10456-prettier)

# Gotchas
- Network domains must be `https`
- If you update the loop's permissions in `package.json` after you've installed your loop, you'll need to manually update `Olive Helps/secureloops/[loop]/metadata.json` and restart Olive Helps.
- A symlink to `loop.js` must be created in order for OH to auto-update the loop. This step was completed in the **Building a local loop** section while running `yarn symlink`
- Every time `loop.js` changes you must click `Refresh` in the **Loop Library**

# Best practices & things to consider
- Avoid global modules, install them as devDependencies. This will avoid env confusion and issues running the loop command.
- If you need to run a dev dependency with npm you can create a npm script in package.json which will automatically find the binary via the npm_modules alias e.g., `nodemon` is the same as `./node_modules/.bin/nodemon`
- If you need to run a dev dependency with npm directly with the CL, outside of package.json you should be able to use npx.
- Create configuration files for each tool(e.g., .eslint, tsconfig.json) to make code easier to read and to avoid long, inline npm scripts.
- Use ES modules and the absolute alias `~` for imports (e.g., `import {module} ~/helpers/api`)
